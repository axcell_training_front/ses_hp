# ローカル環境の作り方
1. voltaをインストールする
  - [windows](https://zenn.dev/longbridge/articles/30c70144c97d32)）
  - [mac](https://zenn.dev/protoout/articles/17-how-to-nodejs-install)）
2. Voltaを使用してnode.js(ver.14)をインストール([参考](https://zenn.dev/longbridge/articles/219f7fa01032a3))
3. ターミナルを開いて、クローンしたリポジトリのディレクトリ（ses_hp）に移動する`$ cd <ses_hpディレクトリのパス>`
4. ターミナルで以下のコマンドを実行`$ npm ci` → node_modulesディレクトリが生成され、gulpなど必要なモジュールがそこにまとめてインストールされます
5. ターミナルで以下のコマンドを実行しgulpを起動する`$ npx gulp`→ローカルサーバーが立ち上がり、ファイルを保存するたびに自動的にejs,sass,jsがコンパイルされ、ブラウザがリロードされます。終了させたい時は`control + C`キーを押します。
<br>

## gulpでやっていること
- Sass→CSSのコンパイル
- ベンダープレフィクス自動追加
- EJS→htmlのコンパイル
- 複数jsファイルをページごとにまとめる(バンドルする)
- javaScriptの文法チェック
- ES6をES5に変換
- ファイルの編集時自動的に保存＋ブラウザリロード

# マークアップの進め方
## 基本
- srcディレクトリ → 実際に記述をするファイル達。
- distディレクトリ → srcを元に生成される本番ファイル。
- 画像ファイルなどのパスは**ルート相対パス**で記述してください。distディレクトリがルートなので、`”/img/logo.png”`のようになります
## html
- テンプレートエンジンとしてEJSを使用しています
- src/ejs/配下にファイルがあります
- ヘッダー、フッターなどはパーツごと切り分けてincludeファイルとして管理しています。（src/ejs/include/配下）
- src/ejs/pages/配下が各ページのファイルです
<br>

## Sass
- 役割ごとに細かくファイルを分けています。
- ページ固有のCSSはsrc/sass/object/project/の`各ページ名.scss`に記述
<br>

## js
- ページ固有のjs(jQuery)はsrc/js/の`各ページ名.js`に記述
- 共通のjsがあればsrc/js/assets/の`common.js`に記述