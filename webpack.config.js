const path = require("path");
const assets = path.join(__dirname, "/");
const dist = assets + "dist/";

module.exports = {
  entry: {
    top: './src/js/top/top.js',
    service: './src/js/service/service.js',
    company: './src/js/company/company.js',
    contact: './src/js/contact/contact.js',
    recruit: './src/js/recruit/recruit.js'
  },
  output: {
      path: dist,
      filename: 'js/[name]/[name].js'
  },
  mode: 'production',
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          fix: false,
          failOnError: true,
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env',
              ]
            }
          }
        ]
      }
    ]
  },
  target: ["web", "es5"]
  // ,
  // plugins: [
  //   new HtmlWebpackPlugin({
  //     chunks: ['top'],
  //     title: 'トップ',
  //     template: './src/index.html',
  //     filename: 'index.html',
  //   }),
  //   new HtmlWebpackPlugin({
  //     chunks: ['service'],
  //     title: '事業内容',
  //     template: './src/service/index.html',
  //     filename: 'service/index.html',
  //   }),
  //   new HtmlWebpackPlugin({
  //     chunks: ['company'],
  //     title: '会社概要',
  //     template: './src/company/index.html',
  //     filename: 'company/index.html',
  //   }),
  //   new HtmlWebpackPlugin({
  //     chunks: ['contact'],
  //     title: 'お問い合わせ',
  //     template: './src/contact/index.html',
  //     filename: 'contact/index.html',
  //   }),
  //   new MiniCssExtract({
  //     filename: 'css/[name]/[name].[hash].css'
  //   }),
  // ]
};