const gulp = require('gulp');
const sassGlob = require("gulp-sass-glob");
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const webpackStream = require("webpack-stream");
const webpack = require("webpack");
const webpackConfig = require("./webpack.config");
const rename = require("gulp-rename");
const ejs = require("gulp-ejs");
const replace = require("gulp-replace");
const browserSync = require('browser-sync');

gulp.task('sass', () => {
  const sass = require('gulp-sass');
  return gulp
        .src('./src/sass/**/**/*.scss', {base: './src/sass/object/project'})
        .pipe(sassGlob())
        .pipe(sass())
        .pipe( postcss([ autoprefixer(
          {
            // ☆IEは11以上、Androidは4.4以上
            // その他は最新2バージョンで必要なベンダープレフィックスを付与する
            "overrideBrowserslist": ["last 2 versions", "ie >= 11", "Android >= 4"],
            cascade: false}
        ) ]) )
        .pipe(gulp.dest('./dist/css'))
})

gulp.task("webpack", function(done){
  webpackStream(webpackConfig, webpack)
    .on('error', function (e) {
      this.emit('end');
    })
    .pipe(gulp.dest('./dist'));
  done();
  console.log("gulptest");
});

gulp.task("ejs", (done) => {
  gulp
    .src(["./src/ejs/pages/**/*.ejs", "!" + "./src/ejs/include/**/_*.ejs"])
    .pipe(ejs({}, {}, {ext:'.html'}))
    .pipe(rename({ extname: ".html" }))
    .pipe(replace(/[\s\S]*?(<!DOCTYPE)/, "$1"))
    .pipe(gulp.dest("./dist"));
  done();
});

gulp.task('serve', done => {
  browserSync.init({
    server: {
      port: 8080,
      baseDir: './dist',
      index: 'index.html',
    },
  })
  done()
})

gulp.task('browserReload', (done) => {
  browserSync.reload();
  done();
});

gulp.task('watch', done => {
  gulp.watch('./src/sass/**/**/*.scss', gulp.series('sass'));
  gulp.watch('./src/sass/**/**/*.scss', gulp.task('browserReload'));
  gulp.watch('./src/js/**/**/*.js', gulp.series('webpack'));
  gulp.watch('./src/js/**/**/*.js', gulp.task('browserReload'));
  gulp.watch('./src/ejs/**/**/*.ejs', gulp.series('ejs'));
  gulp.watch('./src/ejs/**/**/*.ejs', gulp.task('browserReload'));
  done()
})

gulp.task('default', gulp.series('serve', 'watch'))
// gulp.task('default', gulp.series(gulp.parallel('serve', 'watch')))