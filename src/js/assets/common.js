'use strict';
$(() => {
  const menu = () => {
    const $hamburgerMenu = $('.l-hamburgerMenu');
    const $headerSp = $('.l-header__sp');
    const $spIndex = $('.c-link__header-ham');
    $hamburgerMenu.on('click', () => {
      show();
    });
    $headerSp.on('click', () => {
      hide();
    });
    $spIndex.on('click', () => {
      hide();
    });
    const show = () => {
      $headerSp.stop(true,true).slideToggle();
    };
    const hide = () => {
      $headerSp.slideUp();
    };
  };
  menu();

  const resize = () => {
    const $headerSp = $('.l-header__sp');
    $(window).resize(() => {
      change();
    });
    const change = () => $headerSp.css('display', 'none');
  };
  resize();

  const scroll = () => {
    const urlHash = location.hash;

    if(urlHash) {
      $('body, html').stop().scroll();
      setTimeout(() => {
        scrollToAnker(urlHash)
      }, 100);
    }

    $('a[href^="#"]').on('click', function(event) {
      event.preventDefault();
      const decodedHash = decodeURI(this.hash);
      const hash = decodedHash == "#" || decodedHash == "" ? 'html' : decodedHash;
      scrollToAnker(hash);

      return false;
    });

    const scrollToAnker = (hash) => {
      const $header = $('.l-header__inner');
      const headerHeight = $header.height();
      const adjust = headerHeight;
      const target = $(hash);
      const speed = 500;
      const position = target.offset().top - adjust;
  
      $('html, body').animate({
        scrollTop: position
      }, speed, 'swing');
    }
  };
  scroll();

});
