import "./../assets/common.js";

$(() => {
  //start 送信ボタンの有効化無効化の処理
  // 「同意する」のチェックボックスを取得
  const agreeCheckbox = $('#form_privacy');
  const submitBtn = $(".p-contact__submit");

  // チェックボックスをクリックした時
  agreeCheckbox.on("click", (e) => {
    const _this = e.currentTarget;
    // チェックされている場合
    if ($(_this).prop("checked")) {
      submitBtn.removeClass("p-contact__submit--disable").prop('disabled', false); // disabledを外す
    }
    // チェックされていない場合
    else {
      submitBtn.addClass("p-contact__submit--disable").prop('disabled', true); // disabledを付与
    }
  });
  //end 送信ボタンの有効化無効化の処理

  // メールのバリデーション？
  function validateMail(){
    const form = $("form")
    console.log(form);
    form.attr({
      "novalidate": "",
      "id": "mail_form"
    });
    
  }

  validateMail();

  // 確認ページでの表示の変更
  if($('.mw_wp_form_confirm, .mw_wp_form_complete').length) {
    $('.p-contact__lead').html("以下の内容で送信されます。<span class='p-contact__lead__text'>内容にお間違いがなければ、送信ボタンをクリックしてください。</span>");
    $('.p-contact__selectWrap')
    .css({
        'border': 'none',
        'border-radius': '0px',
        'backgroundColor': 'inherit'
    })
    .addClass('p-contact__selectWrap--confirm');
    $('.p-contact__privacy').css('display', 'none');
    $('.p-contact__submit').removeClass('p-contact__submit--disable');

  }
});