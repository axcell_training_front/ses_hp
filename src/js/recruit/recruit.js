import "../assets/common.js";

//詳細ページ
const readmore = document.querySelectorAll('.c-moreWrap');
Array.from(readmore).forEach((more) => {

  const btn = more.querySelector('.c-more__btn');
  const content = more.querySelector('.c-more__item');
  const height = content.clientHeight;
  const displayWidth = window.screen.width;
  const displayHeight = window.screen.height;
  const wrapHeight = displayWidth / 2;

  if (height <= displayHeight) {
    btn.remove();
    content.style.maxHeight = `${content.scrollHeight}px`;
    more.classList.add('open');
  } else {
    more.style.maxHeight = `${wrapHeight}px`;
    btn.addEventListener('click', () => {
      if (!more.classList.contains('open')) {
        more.style.maxHeight = `${height}px`;
        more.classList.add('open');
        btn.textContent = 'とじる';
        more.style.paddingBottom = '30px';
      } else {
        more.style.maxHeight = `${wrapHeight}px`;
        more.classList.remove('open');
        btn.textContent = 'もっとみる';
      }
    });
  }

});

//エントリーページ
$(() => {
  //start 送信ボタンの有効化無効化の処理
  // 「同意する」のチェックボックスを取得
  const agreeCheckbox = document.getElementById("form_privacy");
  const submitBtn = document.querySelectorAll(".p-entry__submit");

  Array.from(submitBtn).forEach((elem) => {
    // チェックボックスをクリックした時
    agreeCheckbox.addEventListener('click', () => {
      const check = agreeCheckbox.checked;

      if (check === true) {//チェックされている場合
        elem.classList.remove("p-entry__submit--disable");
        elem.disabled = false;//disabledを外す
      } else {//チェックされていない場合
        elem.classList.add("p-entry__submit--disable");
        elem.disabled = true;//disabledを付与
      }

    });
  });
  // end 送信ボタンの有効化無効化の処理

  // メールのバリデーション？
  function validateMail() {
    const form = $("form")
    form.attr({
      "novalidate": "",
      "id": "mail_form"
    });
  }
  validateMail();
  // 確認ページでの表示の変更
  if ($('.mw_wp_form_confirm, .mw_wp_form_complete').length) {
    $('.p-entry__lead').html("以下の内容で送信されます。<span class='p-entry__lead__text'>内容にお間違いがなければ、送信ボタンをクリックしてください。</span>");
    $('.p-entry__selectWrap')
      .css({
        'border': 'none',
        'border-radius': '0px',
        'backgroundColor': 'inherit'
      })
      .addClass('p-entry__selectWrap--confirm');
    $('.p-entry__privacy').css('display', 'none');
    $('.p-entry__submit').removeClass('p-entry__submit--disable');
  }
});
//ここからエントリーフォームのjs
const confirm_screen = document.querySelector('.mw_wp_form_preview');
const complete_screen = document.querySelector(".mw_wp_form_complete");
//confirm_screen.length if文"
const fileLabels_lists = document.querySelectorAll('.p-fileLabel');
const entry_form = document.querySelector('.p-entry__formText__comment');
const file_deletes = document.querySelectorAll('.mwform-file-delete');
const entry_formTexts = document.querySelectorAll('.p-entry__formText.c-inputWrap--form_attached'); // クラス名を修正
const stepbar_items = document.querySelectorAll(".c-stepbar__item");



//nullishで出来るか試して見る
if (complete_screen !== null) { //完了画面の処理記載場所
  const delete_category = document.querySelector(".p-entry__category");
  delete_category.style.display = "none";
  stepbar_items.forEach((stepbar_item, index) => {
    stepbar_items[index].classList.remove("c-stepbar__item--active");
    stepbar_items[2].classList.add("c-stepbar__item--active");

  });
} else if (confirm_screen !== null) {  //確認画面の処理記載場所
  // 画面遷移が行われたときに実行したいJavaScriptコードをここに書きます
  const jsonOBJ_rirekisyo = sessionStorage.getItem("file_attachment_rirekisyo");
  const jsonOBJ_syokureki = sessionStorage.getItem("file_attachment_syokureki");
  const jsonOBJ_other = sessionStorage.getItem("file_attachment_other");
  const getItem_rirekisyo = JSON.parse(jsonOBJ_rirekisyo);
  const getItem_syokureki = JSON.parse(jsonOBJ_syokureki);
  const getItem_other = JSON.parse(jsonOBJ_other);
  const delete_privacy = document.querySelector(".p-entry__privacy");
  const submit_button = document.querySelector(".c-button.p-entry__submit");
  submit_button.style.backgroundColor = "#005c6e";

  delete_privacy.style.display = "none";

  stepbar_items.forEach((stepbar_item, index) => {
    stepbar_items[index].classList.remove("c-stepbar__item--active");
    stepbar_items[1].classList.add("c-stepbar__item--active");
  });
  entry_form.remove();
  entry_formTexts.forEach((entry_formText, index) => {
    const inputElement = fileLabels_lists[index].querySelector('input');
    let inputValue = null;

    if (inputElement) {
      inputValue = inputElement.value;
    }

    if (inputValue === null) {
      entry_formText.style.display = "none";
    } else {
      entry_formText.insertAdjacentText('afterbegin', inputValue); // 正しいメソッドの使用方法に修正
      for (const fileLabel_list of fileLabels_lists) {
        fileLabel_list.style.display = 'none';
      }
      for (const file_delete of file_deletes) {
        file_delete.style.display = 'none';
      }
    }

    // entry_formText.textContent = getItem_syokureki;
    const confirm_input = entry_formText.querySelector('input[type="hidden"]');
    const confirm_name = confirm_input.getAttribute("name");

    switch (confirm_name) {
      case "attached_rirekisyo":
        entry_formText.firstChild.textContent = getItem_rirekisyo;
        break;

      case "attached_syokureki":
        entry_formText.firstChild.textContent = getItem_syokureki;
        break;

      case "attached_other":
        entry_formText.firstChild.textContent = getItem_other;
        break;

      default:
        break;
    }
  });
} else {//フォーム画面の処理記載場所

  const confirm_screen = document.querySelector('.mw_wp_form_preview');
  const complete_screen = document.querySelector(".mw_wp_form_complete");
  //confirm_screen.length if文"
  const fileLabels_lists = document.querySelectorAll('.p-fileLabel');
  const entry_form = document.querySelector('.p-entry__formText__comment');
  const file_deletes = document.querySelectorAll('.mwform-file-delete');
  const entry_formTexts = document.querySelectorAll('.p-entry__formText.c-inputWrap--form_attached'); // クラス名を修正
  const stepbar_items = document.querySelectorAll(".c-stepbar__item");



  //nullishで出来るか試して見る
  if (complete_screen !== null) { //完了画面の処理記載場所
    sessionStorage.removeItem("file_attachment_rirekisyo");
    sessionStorage.removeItem("file_attachment_syokureki");
    sessionStorage.removeItem("file_attachment_other");
    const delete_category = document.querySelector(".p-entry__category");
    delete_category.style.display = "none";
    stepbar_items.forEach((stepbar_item, index) => {
      stepbar_items[index].classList.remove("c-stepbar__item--active");
      stepbar_items[2].classList.add("c-stepbar__item--active");

    });
  } else if (confirm_screen !== null) { //確認画面の処理記載場所
    // 画面遷移が行われたときに実行したいJavaScriptコードをここに書きます
    const jsonOBJ_rirekisyo = sessionStorage.getItem("file_attachment_rirekisyo");
    const jsonOBJ_syokureki = sessionStorage.getItem("file_attachment_syokureki");
    const jsonOBJ_other = sessionStorage.getItem("file_attachment_other");
    const getItem_rirekisyo = JSON.parse(jsonOBJ_rirekisyo);
    const getItem_syokureki = JSON.parse(jsonOBJ_syokureki);
    const getItem_other = JSON.parse(jsonOBJ_other);
    const delete_privacy = document.querySelector(".p-entry__privacy");
    const submit_button = document.querySelector(".c-button.p-entry__submit");
    submit_button.style.backgroundColor = "#005c6e";

    delete_privacy.style.display = "none";

    stepbar_items.forEach((stepbar_item, index) => {
      stepbar_items[index].classList.remove("c-stepbar__item--active");
      stepbar_items[1].classList.add("c-stepbar__item--active");
    });
    entry_form.remove();
    entry_formTexts.forEach((entry_formText, index) => {
      const inputElement = fileLabels_lists[index].querySelector('input');
      let inputValue = null;

      if (inputElement) {
        inputValue = inputElement.value;
      }

      if (inputValue === null) {
        entry_formText.style.display = "none";
      } else {
        entry_formText.insertAdjacentText('afterbegin', inputValue); // 正しいメソッドの使用方法に修正
        for (const fileLabels_list of fileLabels_lists) {
          fileLabels_list.style.display = 'none';
        }
        for (const file_delete of file_deletes) {
          file_delete.style.display = 'none';
        }
      }

      const confirm_input = entry_formText.querySelector('input[type="hidden"]');
      if (confirm_input !== null) {
        const confirm_name = confirm_input.getAttribute("name");
        switch (confirm_name) {
          case "attached_rirekisyo":
            entry_formText.firstChild.textContent = getItem_rirekisyo;
            break;

          case "attached_syokureki":
            entry_formText.firstChild.textContent = getItem_syokureki;
            break;

          case "attached_other":
            entry_formText.firstChild.textContent = getItem_other;
            break;

          default:
            break;
        }
      }

    });
  } else {//フォーム画面の処理記載場所

    const jobTitle = document.querySelector(".p-entry__category");
    const form_job = document.querySelector("#form_job");
    if (form_job) {

      form_job.value = jobTitle.textContent;

    }

    entry_formTexts.forEach((entry_formText) => {
      const selected_file = entry_formText.querySelector(".mw-wp-form_file");
      const file_delete = entry_formText.querySelector(".mwform-file-delete");
      const disply_file_delete = entry_formText.querySelector('.p-entry__fileDelete');
      const inputValue = entry_formText.querySelector("input");
      const jsonOBJ_rirekisyo = sessionStorage.getItem("file_attachment_rirekisyo");
      const jsonOBJ_syokureki = sessionStorage.getItem("file_attachment_syokureki");
      const jsonOBJ_other = sessionStorage.getItem("file_attachment_other");
      const getItem_rirekisyo = JSON.parse(jsonOBJ_rirekisyo);
      const getItem_syokureki = JSON.parse(jsonOBJ_syokureki);
      const getItem_other = JSON.parse(jsonOBJ_other);
      const get_ID_entry_input = entry_formText.querySelector(".p-entry__input");
      const this_id = get_ID_entry_input.getAttribute("id");
      const this_errors = entry_formText.querySelectorAll(".error")
      const this_fileLabel = entry_formText.querySelector(".p-fileLabel");

      const createSpan = (entry_formText, innerCloseBtn, outerCloseBtn, input, event) => {
        const _this = event.currentTarget;
        const parent = _this.closest(".p-entry__formText");
        const get_ID_entry_input = parent.querySelector(".p-entry__input");
        const this_id = get_ID_entry_input.getAttribute("id");
        const fileName = parent.querySelector(".p-fileName");
        const fileLabel_change_color = parent.querySelector(".p-fileLabel");

        if (fileName) {
          fileName.remove();
        }

        const temporarily_span = document.createElement("span");

        temporarily_span.className = "p-fileName";

        fileLabel_change_color.style.backgroundColor = '#4D3B3B33';
        fileLabel_change_color.style.color = "#4D3B3B";

        const value = input.value;

        const valueName = value.replace(/^.*[\\/]/, '');

        temporarily_span.textContent = valueName;

        const json_temporarily_span = JSON.stringify(temporarily_span.textContent);

        if (this_id === "rirekisyo") {

          sessionStorage.setItem("file_attachment_rirekisyo", json_temporarily_span);

        } else if (this_id === "syokureki") {

          sessionStorage.setItem("file_attachment_syokureki", json_temporarily_span);

        } else if (this_id === "other") {

          sessionStorage.setItem("file_attachment_other", json_temporarily_span);

        }
        entry_formText.insertBefore(temporarily_span, outerCloseBtn);
      }
      disply_file_delete.style.display = "none";
      // file_delete.style.visibility ="visible";
      entry_formText.append(file_delete);

      if (this_errors) {

        for (const this_error of this_errors) {
          entry_formText.append(this_error);
        }
      }
      if (selected_file) {
        const temporarily_span = document.createElement("span");
        if (this_id === "rirekisyo") {

          temporarily_span.textContent = getItem_rirekisyo;

        } else if (this_id === "syokureki") {

          temporarily_span.textContent = getItem_syokureki;

        } else if (this_id === "other") {

          temporarily_span.textContent = getItem_other;
        }

        selected_file.style.display = "none";

        file_delete.style.display = "none";

        temporarily_span.className = "p-fileName";

        this_fileLabel.style.backgroundColor = '#4D3B3B33';

        this_fileLabel.style.color = "#4D3B3B";

        entry_formText.insertBefore(temporarily_span, disply_file_delete);
      }
      this_fileLabel.addEventListener("click", (event) => {
        const _this = event.currentTarget;
        const currentInput = _this.querySelector("input");
        currentInput.addEventListener("change", (event) => {
          createSpan(entry_formText, file_delete, disply_file_delete, inputValue, event);
        });
      });
      file_delete.addEventListener("click", (event) => {
        const _this = event.currentTarget;
        const parent = _this.closest(".p-entry__formText");
        const children_length = parent.children.length;
        if (children_length > 3) {
          const this_fileLabel = parent.querySelector(".p-fileLabel");
          const this_fileName = this_fileLabel.nextElementSibling;
          this_fileName.style.display = "none";
          if (this_fileName) {
            this_fileName.style.display = "none";
          }
        }
        const this_fileLabel = entry_formText.querySelector(".p-fileLabel");

        this_fileLabel.style.backgroundColor = '#4d3b3b';

        this_fileLabel.style.color = "#fbfbfb";
      });
    });
  }
}
